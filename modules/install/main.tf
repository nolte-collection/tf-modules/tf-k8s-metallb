
variable "namespace" {
  default = "metallb"
}


resource "kubernetes_namespace" "metallb" {
  metadata {
    name = var.namespace
  }
}
variable "layer2_ip_range" {

}
locals {
  EXTRA_VALUES = {
    configInline = {
      address-pools = [{
        name      = "generic-cluster-pool"
        protocol  = "layer2"
        addresses = [var.layer2_ip_range]
      }]
    }
  }
}
variable "image_tag" {
  default = "v0.9.3"
}

# https://github.com/bitnami/charts/tree/master/bitnami/metallb/#installing-the-chart
resource "helm_release" "release" {
  name       = "metallb"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metallb"
  namespace  = kubernetes_namespace.metallb.metadata[0].name
  values = [
    #"${file("${path.module}/files/values.yaml")}"
    yamlencode(local.EXTRA_VALUES)
  ]

  set {
    name  = "controller.image.repository"
    value = "metallb/controller"
  }
  set {
    name  = "controller.image.tag"
    value = var.image_tag
  }

  set {
    name  = "speaker.image.repository"
    value = "metallb/speaker"
  }
  set {
    name  = "speaker.image.tag"
    value = var.image_tag
  }
}
